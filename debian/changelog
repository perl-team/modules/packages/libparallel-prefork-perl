libparallel-prefork-perl (0.18-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * New upstream release.
  * Update copyright years for inc/Module/*.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.7.
  * Bump debhelper compatibility level to 9.
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.
  * Add debian/upstream/metadata.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Add missing build dependency on libmodule-install-perl.

  [ gregor herrmann ]
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Bump debhelper-compat to 13.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Tue, 24 May 2022 20:55:51 +0200

libparallel-prefork-perl (0.17-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 14:52:31 +0100

libparallel-prefork-perl (0.17-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sat, 12 Jul 2014 19:28:00 +0200

libparallel-prefork-perl (0.15-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Strip trailing slash from metacpan URLs.
  * Add (build) dependency on libsignal-mask-perl.

 -- gregor herrmann <gregoa@debian.org>  Wed, 14 May 2014 19:33:36 +0200

libparallel-prefork-perl (0.14-1) unstable; urgency=low

  * Team upload.

  [ gregor herrmann ]
  * Add copyright information for inc/Module/*.
    Thanks to Ansgar Burchardt for the pointer.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * Don't install README anymore.
    Just a text version of the POD/manpage.
  * debian/copyright: add comment regarding upstream copyright holder.
  * Remove versions from (build) dependencies.
    Nothing older on the archive.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 22 Dec 2013 23:08:11 +0100

libparallel-prefork-perl (0.13-1) unstable; urgency=low

  * Initial Release. (Closes: #693646)

 -- Dominic Hargreaves <dom@earth.li>  Sun, 18 Nov 2012 22:09:16 +0000
